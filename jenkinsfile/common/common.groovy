
def notify_result_to_slack(channel){
        echo "Publish Robot Framework test results"
        def prefix = ":white_check_mark:"
        def failed_count = tm('${ROBOT_FAILED}').toInteger()
        def passed_count = tm('${ROBOT_PASSED}').toInteger()
        if (tm('${ROBOT_FAILED}').toInteger() > 0) {
            prefix = ":firecracker:"
        } 
        def robotlog = "${BUILD_URL}" + "/robot/report/log.html"
        robotlog = robotlog.replaceAll( 'localhost', '10.104.131.135' )
        echo "${robotlog}"
        def full_msg = "${prefix}  ${JOB_NAME}  #${BUILD_NUMBER} \n *BRANCH* : ${GIT_BRANCH} \n passed : ${passed_count} \n failed : ${failed_count} \n after ${currentBuild.durationString} \n (<${robotlog}|Report>)"
        env.NOTI_MSG = "${full_msg}"
        slackSend(channel: "${channel}", message:  "${full_msg}")

}// end notify_result_to_slack

def notify_result_to_line(lineToken){
        echo "sending result to line"
        def failed_count = tm('${ROBOT_FAILED}').toInteger()
        def passed_count = tm('${ROBOT_PASSED}').toInteger()
        def robotlog = "${BUILD_URL}" + "/robot/report/log.html"
        robotlog = robotlog.replaceAll( 'localhost', '111.123.122.88' )
        sh """curl --location --request POST 'https://notify-api.line.me/api/notify' \
        --header 'Authorization: Bearer sQrWNzlOqt0SA7XVtW8S8aYblF2OkgawTeJlryMmve1' \
        --header 'Content-Type: application/x-www-form-urlencoded' \
        --data-urlencode 'message= ${JOB_NAME}
        PASS : ${passed_count}  
        FAIL : ${failed_count} 
        URL : ${robotlog}'"""
}

return this