*** Settings ***
Resource         ${CURDIR}/../resources/web/imports.robot
Test Teardown    common_keywords.Close Store Accounting Website

*** Test Cases ***
Tc workflow case 06
    [Tags]      TC06     web
    [Documentation]      Tc workflow case 06 full flow
    
    common_feature.Open Store Accounting Website and go to home page      ${user['username']}      ${user['password']}     ${module['store_audit']}    ${menu['sam_6200']}        
    home_feature.Input infomation on home page     ${tc_32['date_from']}     ${tc_32['date_to']}    ${tc_32['area']}    ${tc_32['store']}
    home_page.Verify kundsan can be check
    home_page.Verify Exta can be check
    home_page.Verify status confirm datat can be check
    home_page.Verify counting sampling can be check
    home_page.Click search button
    detail_page.Click Text

    