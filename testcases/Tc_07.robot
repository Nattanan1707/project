*** Settings ***
Resource         ${CURDIR}/../resources/web/imports.robot
Test Teardown    common_keywords.Close Store Accounting Website

*** Test Cases ***
Tc workflow case 07
    [Tags]      TC07     web
    [Documentation]      Tc workflow case 07 full flow
    
    common_feature.Open Store Accounting Website and go to home page      ${user['username']}      ${user['password']}     ${module['store_audit']}    ${menu['sam_6200']}        
    home_feature.Input infomation on home page     ${tc_33['date_from']}     ${tc_33['date_to']}    ${tc_33['area']}    ${tc_33['store']}
    home_page.Verify Exta can be check
    home_page.Verify status import can be check
    home_page.Verify counting sampling can be check
    home_page.Click search button
    detail_page.Click Text