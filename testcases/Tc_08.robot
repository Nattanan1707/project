*** Settings ***
Resource         ${CURDIR}/../resources/web/imports.robot
Test Teardown    common_keywords.Close Store Accounting Website

*** Test Cases ***
Tc workflow case 08
    [Tags]      TC08     web
    [Documentation]      Tc workflow case 08 full flow
    
    common_feature.Open Store Accounting Website and go to home page      ${user['username']}      ${user['password']}     ${module['store_audit']}    ${menu['sam_6200']}        
    home_feature.Input infomation on home page     ${tc_34['date_from']}     ${tc_34['date_to']}    ${tc_34['area']}    ${tc_34['store']}
    home_page.Verify Exta can be check
    home_page.Verify status correct data can be check
    home_page.Verify counting normal can be check
    home_page.Click search button
    detail_page.Click Text
    