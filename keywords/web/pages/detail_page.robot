*** Variables ***
${C}        //*[@id="form1"]/table[2]/tbody/tr/td/table[2]/tbody/tr/td/table/tbody/tr
# ${PATH}     ${CURDIR}/testdata/test_detail.yaml
${detail_page.click_text}        xpath=//*[@id="form1:table1:0:link1"] 
${detail_page.back_button}           xpath=//*[@id="form1:back"]


*** Keywords ***
Click Text
    [Documentation]     Click Text
    SeleniumLibrary.Click Element    ${detail_page.click_text} 

Click back button
    [Documentation]     Click back button
    SeleniumLibrary.Click Element    ${detail_page.back_button} 

Get Detail
    [Documentation]     Get Detail
    ${O}    SeleniumLibrary.Get Element Count     ${C}
    @{textList}=    Create List
    FOR     ${U1}     IN RANGE    ${O}-1
            ${U}    Evaluate    ${U1}+1
            Sleep    1
            ${G}    SeleniumLibrary.Get Text    //*[@id="SPAN_General0${U}"]/td[2]
            Append To List        ${textList}       ${G} 
    END
    Log To Console    ${textList}




    
    


    
    